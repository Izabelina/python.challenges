import easygui as gui
from PyPDF2 import PdfReader, PdfWriter

# 1. Ask the use to select a PDF file to open

input_file_path = gui.fileopenbox(title="Select a PDF to extract...", default="*.pdf")

# 2. If no PDF file is chosen, exit the program

if input_file_path is None:
    exit()

# 3. Ask for a starting page number

start_page = gui.enterbox(msg="Enter start page...", title="Start page")

# 4. If the user does not enter a starting page number, exit the program.

if start_page is None:
    exit()

# 5. Valid page numbers are positive integers. If the user enters an invalid page number:
    # • Warn the user that the entry is invalid
    # • Return to step 3

#start_page = int(start_page)

while (not start_page.isdigit() or int(start_page) <= 0 or int(start_page) > len(input_file_path)):
    msg = gui.msgbox(msg="Enter correct number...", title="Upss! Try again...")
    start_page = gui.enterbox(msg="Enter start page...", title="Start page")
      
# 6. Ask for an ending page number    

end_page = gui.enterbox(msg="Enter end page...", title="End page")

# 7. If the user does not enter an ending page number, exit the program

if end_page is None:
    exit()

# 8. If the user enters an invalid page number:
    # • Warn the user that the entry is invalid
    # • Return to step 6
    
while (not end_page.isdigit() or end_page < start_page or int(end_page) > len(input_file_path)):
    msg = gui.msgbox(msg="Enter correct number...", title="Upss! Try again...")
    end_page = gui.enterbox(msg="Enter end page...", title="End page")

# 9. Ask for the location to save the extracted pages

target_file_path = gui.filesavebox(title="Select a folder to save the new file...", default="*.pdf")

# 10. If the user does not select a save location, exit the program

if target_file_path is None:
    exit()

# 11. If the chosen save location is the same as the input file path:
        #• Warn the user that they can not overwrite the input file
        #• Return to step 9

if target_file_path == input_file_path:
    msg = gui.msgbox(msg="You can not overwrite the input file...", title="Upss! Try again...")
    target_file_path = gui.filesavebox(title="Select a folder to save the new file...", default="*.pdf")
    
# 12. Perform the page extraction:
    # • Open the input PDF file.
    # • Write a new PDF file containing only the pages in the selected page range.

output_PDF = PdfWriter()
reader = PdfReader(input_file_path)

for page_num in range(int(start_page) - 1, int(end_page)):
    page = reader.pages[page_num] 
    output_PDF.add_page(page)

with open(target_file_path, "wb") as output_file:
    output_PDF.write(output_file)






     
    
