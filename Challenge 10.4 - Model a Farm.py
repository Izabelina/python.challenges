class Zwierzęta:
    
    jajka = 0
    jaja_na_ciasto = 0
    mleko = 0
    na_ciasto = 0
    
    def __init__(self, rodzaj):
        self.rodzaj = rodzaj
        print(f"To jest {rodzaj}.")

    def żyje(self):
        print(f"Mieszka na farmie.")


class Kura(Zwierzęta):
    
    def __init__(self, imię):
        self.imię = imię
        print(f"Ta kura nazywa się {imię}.")

    def znieś_jajko(self):
        self.jajka = self.jajka + 1
        print(f"Właśnie zniosła jajko. Teraz jest ich {self.jajka}.")
        if self.jajka == 4:
            self.jajka = self.jajka - 3
            self.jaja_na_ciasto = self.jaja_na_ciasto + 1
            print(f"Wystarczy na jajecznicę, ale {self.jajka} odłóż na ciasto.")
            print(f"Na ciasto: {self.jaja_na_ciasto} jaja.")
            if self.jaja_na_ciasto == 4:
                self.jaja_na_ciasto = self.jaja_na_ciasto - 4
                print("Super! Wystarczy jaj na ciasto!")
        else:           
            print("To jeszcze za mało...")
            
   
class Krowa(Zwierzęta):
    
    def __init__(self, imię):
        self.imię = imię
        print(f"Ta krowa nazywa się {imię}.")

    def daj_mleko(self):
        self.mleko = self.mleko + 1
        print(f"Wydoiliśmy krowę i mamy {self.mleko} litry mleka.")
        if self.mleko == 4:
            self.mleko = self.mleko - 3
            self.na_ciasto = self.na_ciasto + 1
            print(f"Wystarczy do sera, ale {self.mleko} odłóż na ciasto.")
            print(f"Na ciasto: {self.na_ciasto} litry mleka.")
            if self.na_ciasto == 4:
                self.na_ciasto = self.na_ciasto - 4
                print("Super! Wystarczy mleka na ciasto!")
        else:           
            print("To jeszcze za mało na ciasto i ser...")
            
    def sernik(self):
        jaja_na_ciasto = 0
        na_ciasto = 0
        if jaja_na_ciasto >= 3 and na_ciasto >= 3:
            print("Pieczemy sernik!")
      

class Pies(Zwierzęta):
   
    def __init__(self, imię):
        self.imię = imię
        print(f"Ten pies nazywa się {imię}.")

    def daj_głos(self):
        print("Dżon, przywitaj się... \nHau hau hauuu...")    
        
kura = Kura('Benia')
kura.żyje()
kura.znieś_jajko()
krowa = Krowa('Lila')
krowa.daj_mleko()
krowa.sernik()
krowa.sernik()
pies = Pies('Dżon')
pies.daj_głos()


