from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

def get_page_text(page):
    return page.extractText()

pdf_path = Path.cwd() / "scrambled.pdf"

pdf_reader = PdfFileReader(str(pdf_path))
pdf_writer = PdfFileWriter()
pages = list(pdf_reader.pages)
pages.sort(key=get_page_text)



for page in pdf_reader.pages:
    if page["/Rotate"] == -90:
        page.rotateClockwise(90)
        pdf_writer.addPage(page)
    elif page["/Rotate"] == 90:
        page.rotateClockwise(-90)
        pdf_writer.addPage(page)
    elif page["/Rotate"] == 180:
        page.rotateClockwise(-180)
        pdf_writer.addPage(page)
    elif page["/Rotate"] == -180:
        page.rotateClockwise(180)
        pdf_writer.addPage(page)
    else:
        page.rotateClockwise(0)
        pdf_writer.addPage(page)

with Path("unscrumbled.pdf").open(mode="wb") as output_file:
    pdf_writer.write(output_file)



