Nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
"extrovert", "gorilla"]
Verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
"explodes", "curdles"]
Adjectives = ["furry", "balding", "incredulous", "fragrant",
"exuberant", "glistening"]
Prepositions = ["against", "after", "into", "beneath", "upon",
"for", "in", "like", "over", "within"]
Adverbs = ["curiously", "extravagantly", "tantalizingly",
"furiously", "sensuously"]

import random
from random import choice
import tkinter as tk

window = tk.Tk()

lbl_name = ["Nouns:",
          "Verbs:",
          "Adjectives:",
          "Prepositions:",
          "Adverbs:"]

frame_labels = tk.Frame(master=window)
for lbl in lbl_name:
    label = tk.Label(window, text=lbl)
    entry = tk.Entry(window)
    label.grid()
    entry.grid()
frame_labels.grid()

button = tk.Button(text="Generate", width=20, heigh=1, bg="black", fg="white")
button.grid()

frame_poem = tk.Frame(master=window)
lbl_poem = tk.Label(master=frame_poem, text="Your poem:")
lbl_poem.grid(pady=10)
frame_poem.grid()

events_list = []

def handle_keypress(event):
    print(event.char)

def make_poem():
    """Create a randomly generated poem, returned as a multi-line string."""
    # Pull three nouns randomly
    noun1 = random.choice(Nouns)
    noun2 = random.choice(Nouns)
    noun3 = random.choice(Nouns)
    # Make sure that all the nouns are different
    while noun1 == noun2:
        noun2 = random.choice(Nouns)
    while noun1 == noun3 or noun2 == noun3:
        noun3 = random.choice(Nouns)

    # Pull three different verbs
    verb1 = random.choice(Verbs)
    verb2 = random.choice(Verbs)
    verb3 = random.choice(Verbs)
    while verb1 == verb2:
        verb2 = random.choice(Verbs)
    while verb1 == verb3 or verb2 == verb3:
        verb3 = random.choice(Verbs)

    # Pull three different adjectives
    adjctv1 = random.choice(Adjectives)
    adjctv2 = random.choice(Adjectives)
    adjctv3 = random.choice(Adjectives)
    while adjctv1 == adjctv2:
        adjctv2 = random.choice(Adjectives)
    while adjctv1 == adjctv3 or adjctv2 == adjctv3:
        adjctv3 = random.choice(Adjectives)

    # Pull two different prepositions
    prep1 = random.choice(Prepositions)
    prep2 = random.choice(Prepositions)
    while prep1 == prep2:
        prep2 = random.choice(Prepositions)

    # Pull one adverb
    adv1 = random.choice(Adverbs)

    if "aeiou".find(adjctv1[0]) != -1:  # First letter is a vowel
        article = "An"
    else:
        article = "A"

    # Create the poem
    poem = (
        f"{article} {adjctv1} {noun1}\n\n"
        f"{article} {adjctv1} {noun1} {verb1} {prep1} the {adjctv2} {noun2}\n"
        f"{adv1}, the {noun1} {verb2}\n"
        f"the {noun2} {verb3} {prep2} a {adjctv3} {noun3}"
    )
    return poem

poem = make_poem()
print(poem)

window.mainloop()

