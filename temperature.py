# zadanie 1

def convert_cel_to_far(Celsius):
    '''(int)->float
    Returns the same temperature in Fahrenheit's degrees'''
    Fahrenheit = Celsius * 9/5 + 32
    return Fahrenheit

celsius_user = input("Enter a temperature in degrees C: ")
celsius_user = int(celsius_user)

print(f"{celsius_user} degrees C = {convert_cel_to_far(celsius_user):.2f} degrees F")



# zadanie 2:

def convert_far_to_cel(Fahrenheit):
    '''(int)->float
    Returns the same temperature in Celsius's degrees'''
    Celsius = (Fahrenheit - 32) * 5/9
    return Celsius

fahrenheit_user = input("Enter a temperature in degrees F: ")
fahrenheit_user = int(fahrenheit_user)

print(f"{fahrenheit_user} degrees F = {convert_far_to_cel(fahrenheit_user):.2f} degrees C")





