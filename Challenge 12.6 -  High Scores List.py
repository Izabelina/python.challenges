
import pathlib
from pathlib import Path
import csv


path = Path.home() / "python.challenges" / "practice_files" / "scores.csv"

with path.open(mode="r", encoding="utf-8") as file:
    reader = csv.DictReader(file)
    scores = [row for row in reader]
    print(scores)

high_scores = {}
for item in scores:
    name = item["name"]
    score = int(item["score"])

    if name not in high_scores:
        high_scores[name] = score

    else:
        if score > high_scores[name]:
            high_scores[name] = score

csv_file = Path.cwd() / "high_scores.csv"
with csv_file.open(mode="w", encoding="utf-8") as file:
    writer = csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for name in high_scores:
        row_dict = {"name": name, "high_score": high_scores[name]}
        writer.writerow(row_dict)
