# 9.9 - Challenge: Cats With Hats
'''
You have 100 cats.
One day you decide to arrange all your cats in a giant circle. Initially,
none of your cats have any hats on. You walk around the circle 100
times, always starting at the same spot, with the first cat (cat # 1). Every time you stop at a cat, you either put a hat on it if it doesn’t have
one on, or you take its hat off if it has one on.
1. The first round, you stop at every cat, placing a hat on each one.
2. The second round, you only stop at every second cat (#2, #4, #6,
#8, etc.).
3. The third round, you only stop at every third cat (#3, #6, #9, #12,
etc.).
4. You continue this process until you’ve made 100 rounds around
the cats (e.g., you only visit the 100th cat).
Write a program that simply outputs which cats have hats at the end
'''

# zmniejszyłam ilość kotów i okrążeń do 10 - żeby zobaczyć w debuggerze jak to idzie.

number_of_cats = 10
cats_with_hats = []
number_of_laps = 10

# We want the laps to be from 1 to 10 instead of 0 to 9
for lap in range(1, number_of_laps + 1):
    for cat in range(1, number_of_cats + 1):

        # Only look at cats that are divisible by the lap
        if cat % lap == 0:
            if cat in cats_with_hats:
                cats_with_hats.remove(cat)
            else:
                cats_with_hats.append(cat)

print(cats_with_hats)

'''
jeśli kot w kolejce jest podzielny przez numer okrążenia, to jest wywalany z listy
kotów w kapeluszu. jeśli nie jest podzielny - zostaje na liście.
'''
