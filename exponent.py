base_num = input("Enter a base: ")
pow_num = input("Enter an exponent: ")
base_num_to_power_num = float(base_num)**float(pow_num)

print(f"{base_num} to the power of {pow_num} = {base_num_to_power_num}")
