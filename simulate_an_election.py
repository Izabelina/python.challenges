'''
Startują dwaj kandydaci, Kandydat A i Kandydat B dla burmistrza w mieście
z trzema regionami głosowania. Wg sondaży Kandydat A ma w każdym z nich
następujące szanse na wygraną:
• Region 1: 87% szans na wygraną
• Region 2: 65% szans na wygraną
• Region 3: 17% szans na wygraną

Napisz program, który symuluje wybory 10 000 razy i drukuje procent, w którym
wygrywa Kandydat A.
Aby uprościć sprawę, załóżmy, że kandydat wygrywa wybory, jeśli wygra
w co najmniej dwóch z trzech regionów.
'''
from random import random

num_wins_A = 0
num_wins_B = 0

trials = 10000

for trial in range(trials):
    candidate_A_votes = 0
    candidate_B_votes = 0
    if random() < 0.87:
        candidate_A_votes = candidate_A_votes + 1
    else:
        candidate_B_votes = candidate_B_votes + 1        
    if random() < 0.65:
        candidate_A_votes = candidate_A_votes + 1
    else:
        candidate_B_votes = candidate_B_votes + 1        
    if random() < 0.17:
        candidate_A_votes = candidate_A_votes + 1
    else:
        candidate_B_votes = candidate_B_votes + 1
        
    if candidate_A_votes > candidate_B_votes:
        num_wins_A = num_wins_A + 1
    else:
        num_wins_B = num_wins_B + 1
        
print(f"Candidate A has {(num_wins_A / trials) * 100} % chances to win.")
print(f"Candidate B has {(num_wins_B / trials) * 100} % chances to win.")
