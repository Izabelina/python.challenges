# moje rowiązanie:

def invest(amount, rate, year):
    '''(float, float, int) -> float
    100.00 = 100.00 + (100.00 * .05)'''

    amount = amount + (amount * rate)
    return (amount)

amount = float(input("Enter an amount: "))
rate = float(input("Enter a rate: "))
year = int(input("Enter a number of years: "))

for year in range(1, year+1):
    amount = invest(amount, rate, year)
    print(f"year {year} = {(amount):.2f} $")





# rozwiązanie z Python Basic Book:

def invest(amount, rate, years):
    """Display year on year growth of an initial investment"""
    for year in range(1, years + 1):
        amount = amount * (1 + rate)
        print(f"year {year}: ${amount:,.2f}")

amount = float(input("Enter a principal amount: "))
rate = float(input("Enter an anual rate of return: "))
years = int(input("Enter a number of years: "))

invest(amount, rate, years)
