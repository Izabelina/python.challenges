from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter

class PdfFileSplitter:
    def __init__(self, path):
        ''' metoda init jest uruchamiana w momencie, gdy tworzymy obiekt
        zawiera atrybuty obiektu, które mogą być różne dla poszczególnych instancji'''
        self.path = PdfFileReader(path)
        '''Inicjujemy 2 atrybuty, które na razie jeszcze nie będą potrzebne, 
        bo plik nie został jeszcze podzielony. Dopiero metoda split będzie tworzyłą
        dwie różne części. Teraz robimy tylko miejsce w klasie na to, żeby potem 
        nie kombinować'''
        self.writer1 = None
        self.writer2 = None
    
    def split(self, breakpoint):
        '''Dzieli PDF na 2 instancje PdfFileWriter. Dla atrybutów 1 i 2
        ustawiamy nową instancję PdfFileWriter()'''
        self.writer1 = PdfFileWriter()
        self.writer2 = PdfFileWriter()
        '''do atrybutu writer1 dodajemy wszystkie strony z zakresu 
        od początku do breakpointu (bez breakpoint) a do writer2 pozostałe'''
        for page in self.path.pages[:breakpoint]:
            self.writer1.add_page(page)
        for page in self.path.pages[breakpoint:]:
            self.writer2.add_page(page)

    def write(self, filename):
        '''zapisyje obie instancje PfFFileWriter do plików'''
        with Path(filename + "_1.pdf").open(mode="wb") as output_file:
            var = self.writer1.write(output_file)
        with Path(filename + "_2.pdf").open(mode='wb') as output_file:
            var = self.writer2.write(output_file)
        
split_pdf = PdfFileSplitter(Path.home()/"PYTHON/python.challenges/Pride_and_Prejudice.pdf")
split_pdf.split(breakpoint=150)
split_pdf.write("Pride_part")








    
pdf_path = (Path.home()/'PYTHON'/'python.challenges'/'Pride_and_Prejudice')
pdf_splitter = PdfFileSplitter('Pride_and_Prejudice.pdf')
