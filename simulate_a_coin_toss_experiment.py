from random import randint

def coin_flip():
    '''Randomly return 'heads' or 'tails'.'''
    if randint(0,1) == 0:
        return "Heads"
    else:
        return "Tails"

all_trials = 10000
total_flips = 0

for trial in range(all_trials):
    if coin_flip() == "Heads":
        total_flips = total_flips + 1
        while coin_flip() == "Heads":
            total_flips = total_flips + 1
        total_flips = total_flips + 1
    else:
        total_flips = total_flips + 1
        while coin_flip() == "Tails":
            total_flips = total_flips + 1
        total_flips = total_flips + 1
       
average_flips_on_trial = total_flips / all_trials
print(f"The average number of flips on trial is {average_flips_on_trial}.")
